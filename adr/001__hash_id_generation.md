# Hash ID Generation

### Context and Problem Statement

The goal is to comeup with a unique hash generator
while keeping in mind the shortness of the length, hash predictability and
horizontal scaling.


### Considered Options
* [Option 1: Use Auto Increment Integer as identifier](#option-1-db-auto-increment-integer-as-identifier)
* [Option 2: Use Integer as UUID](#option-2-use-integer-as-uuid)
* [Option 3: Use a Hash generator with Unique checker on DB layer](#option-3-use-a-hash-generator-with-unique-checker-on-db-layer)

Chosen option: "Option 3", because:
- while having the shortness of the hash we still have the uniqueness of the hash on the DB level

## Proposed Implementation
- Create a hash generator that would have a max hash lenght of 5.
- Always compare the generated hash to the url and make sure its shorter
- Push the uniqueness constraint on the DB layer to make sure we dont have similar hash
- Create a retry mechanism that repeats the process until uniqueness is obtained.


## Pros and Cons of the Options

### Option 1: Use DB Auto Increment Integer as identifier

* Good, initially the length would be shorter.
* Good, uniqueness
* Bad, identifier predictability and security
* Bad, lesser possible combination

### Option 2: Use Integer as UUID
* Good, uniqueness
* Bad, Length is long

### Option 3: Use a Hash generator with Unique checker on DB layer
* Good, uniqueness
* Good, shorter hash

