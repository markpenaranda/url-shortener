package com.markpenaranda.urlshortener.repositories

import com.markpenaranda.urlshortener.models.UrlRecord

interface UrlRecordRepository {
    fun save(record: UrlRecord)
    fun getUrlByHash(hash: String): String?
}

data class DuplicateHashException(val msg: String?): Exception(msg)