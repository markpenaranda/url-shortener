package com.markpenaranda.urlshortener.repositories

import com.markpenaranda.urlshortener.models.UrlRecord
import org.intellij.lang.annotations.Language
import org.springframework.dao.DuplicateKeyException
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate

class JDBCUrlRecordRepository(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
): UrlRecordRepository {


    override fun save(record: UrlRecord) {
       try {
           jdbcTemplate.update(INSERT_URL_RECORD, mapOf(
               "hash" to record.hash.value,
               "url" to record.url
           ))
       } catch (e: DuplicateKeyException) {
           throw DuplicateHashException(e.message)
       }

    }

    override fun getUrlByHash(hash: String): String? =
       jdbcTemplate.findOne(
           FIND_BY_HASH,
           mapOf("hash" to hash)
       ) { rs, _ -> rs.getString("url")}


    companion object {
        @Language("SQL")
        private val INSERT_URL_RECORD = """
            INSERT INTO url_record (hash, url) VALUES (:hash, :url)
        """.trimIndent()

        @Language("SQL")
        private val FIND_BY_HASH = """
            SELECT url FROM url_record WHERE hash = :hash
        """.trimIndent()
    }
}

fun <T> NamedParameterJdbcTemplate.findOne(
    queryString: String,
    parameters: Map<String, Any>,
    rowMapper: RowMapper<T>
): T? =
    try {
        queryForObject(queryString, MapSqlParameterSource(parameters), rowMapper)
    } catch (e: EmptyResultDataAccessException) {
        null
    }
