package com.markpenaranda.urlshortener.config

import com.markpenaranda.urlshortener.repositories.JDBCUrlRecordRepository
import com.markpenaranda.urlshortener.repositories.UrlRecordRepository
import com.markpenaranda.urlshortener.services.HashService
import com.markpenaranda.urlshortener.services.UrlRecordService
import org.hashids.Hashids
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate

@Configuration
class UrlRecordRepositoryConfig {
    @Bean
    fun urlRecordRepository(namedParameterJdbcTemplate: NamedParameterJdbcTemplate) = JDBCUrlRecordRepository(namedParameterJdbcTemplate)

    @Bean
    fun urlRecordService(
        urlRecordRepository: UrlRecordRepository,
        hashService: HashService
    ) = UrlRecordService(urlRecordRepository, hashService)

    @Bean
    fun hashService(
        @Value("\${app.hash.salt}")
        hashSalt: String,
    ) = HashService(Hashids(hashSalt))
}