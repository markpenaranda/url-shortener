package com.markpenaranda.urlshortener.api.request

data class StoreUrlRequest (val url: String)