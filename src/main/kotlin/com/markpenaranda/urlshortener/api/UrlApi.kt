package com.markpenaranda.urlshortener.api

import com.markpenaranda.urlshortener.api.request.StoreUrlRequest
import com.markpenaranda.urlshortener.services.UrlRecordService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import javax.servlet.http.HttpServletResponse


@RestController
class UrlApi(
    val urlRecordService: UrlRecordService
) {
    @PostMapping("/url")
    fun store(@RequestBody request: StoreUrlRequest) =
        urlRecordService.save(request.url)

    @GetMapping("{hash}")
    fun getFullUrl(@PathVariable hash: String, response: HttpServletResponse) =
        urlRecordService.getFullUrlFromHash(hash)
            ?.let { response.redirectTo(it) }
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "url not found");

    private fun HttpServletResponse.redirectTo(url: String) {
        this.setHeader("Location", url)
        this.status = 301
    }
}