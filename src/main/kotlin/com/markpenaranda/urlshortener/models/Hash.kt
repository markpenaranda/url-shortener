package com.markpenaranda.urlshortener.models

import org.hashids.Hashids

data class Hash(val value: String)