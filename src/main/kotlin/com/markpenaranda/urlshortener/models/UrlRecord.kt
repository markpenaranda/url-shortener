package com.markpenaranda.urlshortener.models

data class UrlRecord (val hash: Hash, val url: String)