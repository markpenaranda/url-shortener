package com.markpenaranda.urlshortener.services

import com.markpenaranda.urlshortener.models.Hash
import org.hashids.Hashids
import java.time.Instant

class HashService(
    private val hashEncoder: Hashids
) {
    fun generateHashFrom(url: String): Hash {
        if(url.length <= 2) {
            throw UrlTooShortException("$url is too short")
        }

        var value = ""
        while (value == "" || value.length >= url.length) {
            val instant = Instant.now().toEpochMilli()
            value = hashEncoder.encode(instant)
            if(value.length > MAX_LENGTH_OF_HASH) {
                value = value.take(MAX_LENGTH_OF_HASH)
            }

            if(value.length >= url.length) {
                value = value.take(url.length - 1)
            }
        }
        return Hash(value)
    }

    companion object {
        const val MAX_LENGTH_OF_HASH = 5
    }
}

data class UrlTooShortException(val msg: String?): IllegalArgumentException(msg)