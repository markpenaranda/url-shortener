package com.markpenaranda.urlshortener.services

import com.markpenaranda.urlshortener.models.Hash
import com.markpenaranda.urlshortener.models.UrlRecord
import com.markpenaranda.urlshortener.repositories.UrlRecordRepository

class UrlRecordService(
    private val urlRecordRepository: UrlRecordRepository,
    private val hashService: HashService
) {

    fun save(url: String): Hash =
        retry {
            val hash = hashService.generateHashFrom(url)
            urlRecordRepository.save(UrlRecord(hash, url))
            hash
        }


    fun getFullUrlFromHash(hash: String) =
        urlRecordRepository.getUrlByHash(hash)

    private fun <T> retry(block: () -> T): T {
        for (i in 0..MAX_RETRIES) {
            try {
                return block()
            } catch (e: Exception) {
                if(i == MAX_RETRIES) {
                    throw e
                }
            }
        }
        throw java.lang.RuntimeException()
    }

    companion object {
        const val MAX_RETRIES = 1000
    }
}