CREATE TABLE url_record (
    hash VARCHAR PRIMARY KEY NOT NULL UNIQUE,
    url VARCHAR
);

CREATE INDEX url_record_hash_idx ON url_record (hash);

