package com.markpenaranda.urlshortener.services

import com.markpenaranda.urlshortener.models.Hash
import com.markpenaranda.urlshortener.models.UrlRecord
import com.markpenaranda.urlshortener.repositories.DuplicateHashException
import com.markpenaranda.urlshortener.repositories.UrlRecordRepository
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test

class UrlRecordServiceTest {

    private val urlRecordRepository = mockk<UrlRecordRepository>(relaxed = true)
    private val hashService = mockk<HashService>(relaxed = true)

    val urlRecordService = UrlRecordService(urlRecordRepository, hashService)

    @Test
    fun `should save url record and return new hash`() {
        // given
        every { hashService.generateHashFrom(URL) } returns HASH

        // when
        urlRecordService.save(URL)

        // then
        verify { urlRecordRepository.save(UrlRecord(HASH, URL)) }
    }

    @Test
    fun `should generate new hash when hash already exists`() {
        // given
        every { hashService.generateHashFrom(URL) } returns HASH
        every { urlRecordRepository.save(UrlRecord(HASH, URL)) } throws DuplicateHashException("test") andThen Unit

        // when
        urlRecordService.save(URL)

        // then
        verify (exactly = 2){ hashService.generateHashFrom(URL)  }
    }

    @Test
    fun `should get the full url by hash`() {
        // given
        every { urlRecordService.getFullUrlFromHash(HASH.value) } returns URL

        // when
        val result = urlRecordService.getFullUrlFromHash(HASH.value)

        // then
        result shouldBe URL
    }

    @Test
    fun `should return null when full url with the identified hash does not exist`() {
        // given
        every { urlRecordService.getFullUrlFromHash(HASH.value) } returns null

        // when
        val result = urlRecordService.getFullUrlFromHash(HASH.value)

        // then
        result shouldBe null
    }

    companion object {
        const val URL = "test-url"
        val HASH = Hash("12345")
    }

}