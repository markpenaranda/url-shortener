package com.markpenaranda.urlshortener.services

import com.markpenaranda.urlshortener.models.Hash
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import org.hashids.Hashids
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.anyLong

class HashServiceTest {

    private val encoder = mockk<Hashids>()

    private val hashService = HashService(encoder)

    @Test
    fun `should generate hash from url`() {
        every { encoder.encode(any()) } returns GENERATED_HASH

        val result = hashService.generateHashFrom(URL)

        result shouldBe Hash(GENERATED_HASH)
    }

    @Test
    fun `should generate hash that is shorter than url`() {
        every { encoder.encode(any()) } returns GENERATED_HASH

        val result = hashService.generateHashFrom(SHORTER_URL)

        result shouldBe Hash(GENERATED_HASH.take(SHORTER_URL.length -1))
    }

    @Test
    fun `should throw exception when url is too short`() {
        every { encoder.encode(any()) } returns GENERATED_HASH

        shouldThrow<UrlTooShortException> {
            hashService.generateHashFrom("aa")
        }
    }

    companion object {
        const val GENERATED_HASH = "12345"
        const val URL = "test-url-long"
        const val SHORTER_URL = "test"
    }
}