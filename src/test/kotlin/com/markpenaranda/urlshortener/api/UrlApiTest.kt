package com.markpenaranda.urlshortener.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.markpenaranda.urlshortener.models.Hash
import com.markpenaranda.urlshortener.services.UrlRecordService
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@WebMvcTest(UrlApi::class)
class UrlApiTest {

    @MockBean
    private lateinit var urlRecordService: UrlRecordService

    @Autowired
    private lateinit var mvc: MockMvc

    @Autowired
    private lateinit var jsonObjectMapper: ObjectMapper

    @Test
    fun `should return 200 when url is successfully stored`() {
        // given
        given(urlRecordService.save(URL)).willReturn(HASH)
        val requestBody = jsonObjectMapper.writeValueAsBytes(
            mapOf(
              "url" to URL
            )
        )

        // when
        mvc.perform(post("/url").contentType(MediaType.APPLICATION_JSON).content(requestBody))
            .andExpect(MockMvcResultMatchers.status().isOk)

    }

    @Test
    fun `should return 400 when full url is not existing in request when storing`() {
        // when
        mvc.perform(post("/url").contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isBadRequest)
    }

    @Test
    fun `should redirect to full url when hash identifier exists`() {
        given(urlRecordService.getFullUrlFromHash(HASH.value)).willReturn(URL)

        mvc.perform(get("/${HASH.value}"))
            .andExpect(MockMvcResultMatchers.status().isMovedPermanently)
            .andExpect(MockMvcResultMatchers.header().stringValues("Location", URL))
    }

    @Test
    fun `should return 400 when full url with hash identifier does not exists`() {
        given(urlRecordService.getFullUrlFromHash(HASH.value)).willReturn(null)

        mvc.perform(get("/${HASH.value}"))
            .andExpect(MockMvcResultMatchers.status().isNotFound)
    }

    companion object {
        val HASH = Hash("12345")
        const val URL = "test-url"
    }
}