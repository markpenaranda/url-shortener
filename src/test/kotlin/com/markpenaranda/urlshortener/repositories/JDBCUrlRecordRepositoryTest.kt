package com.markpenaranda.urlshortener.repositories


import com.markpenaranda.urlshortener.models.Hash
import com.markpenaranda.urlshortener.models.UrlRecord
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import io.mockk.every
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate

@JdbcTest
class JDBCUrlRecordRepositoryTest {

    @Autowired
    private lateinit var jdbcTemplate: NamedParameterJdbcTemplate

    private lateinit var urlRecordRepository: UrlRecordRepository

    @BeforeEach
    internal fun setUp() {
        urlRecordRepository = JDBCUrlRecordRepository(jdbcTemplate)
    }

    @Test
    fun `should save url record`() {
        urlRecordRepository.save(URL_RECORD)

        // then
        urlRecordRepository.getUrlByHash(HASH.value) shouldBe URL
    }

    @Test
    fun `should throw DuplicateHashException when save failed`() {

//        every { jdbcTemplate.save(DB_MODEL) } throws AmazonDynamoDBException("duplicate primary hash key")
//
//        shouldThrow<DuplicateHashException> {
//            urlRecordRepository.save(URL_RECORD)
//
//        }
    }

    companion object {
        val HASH = Hash("12345")
        val URL = "test-url"
        val URL_RECORD = UrlRecord(HASH, URL)
    }
}