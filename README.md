# URL Shortener

Goal is to create a url shortener

### Running Server

```bash 
docker-compose up && ./gradlew bootRun --args='--spring.profiles.active=local'
```

### Endpoints 

#### Saving URL 

Request
```http request
POST: /url
```

```json
{
  "url": "http://google.com"
}
```
Response 
Endpoint returns a generated hash 
```json
{
	"value": "XPa4k"
}
```

#### Resolving URL
```http request
GET: /{hash}
```

Response
User will be redirected to the associtated URL


### ADR 
Architectural decisions are recorded in `/adr` folder

* [1. Hash ID Generation](adr/001__hash_id_generation.md)